package pt.com.scrumify.controllers;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.isA;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import pt.com.scrumify.annotations.UnitTest;
import pt.com.scrumify.configuration.MockScrumifyUser;
import pt.com.scrumify.entities.WorkItemView;
import pt.com.scrumify.helpers.ConstantsHelper;

@DisplayName("Test of work items controller")
@AutoConfigureMockMvc
@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class WorkItemsControllerTest {
   @Autowired
   private MockMvc mvc;
   
   @Test
   @UnitTest
   @Order(1)
   @MockScrumifyUser(username = "anonymous")  
   public void listByNotAllowedUser() throws Exception {
      mvc.perform(get(ConstantsHelper.MAPPING_WORKITEMS))
         .andExpect(status().isForbidden());
   }
   
   @Test
   @UnitTest
   @Order(2)
   @MockScrumifyUser(username = "anonymous")
   public void createByNotAllowedUser() throws Exception {
      int workItem = 1;
      
      mvc.perform(get(ConstantsHelper.MAPPING_WORKITEMS_CREATE + ConstantsHelper.MAPPING_SLASH + workItem))
         .andExpect(status().isForbidden());
   }
   
   @Test
   @UnitTest
   @Order(3)
   @MockScrumifyUser(username = "projectdirector")  
   public void createByAllowedUser() throws Exception {
      int workItem = 1;
      mvc.perform(get(ConstantsHelper.MAPPING_WORKITEMS_CREATE + ConstantsHelper.MAPPING_SLASH + workItem))
         .andExpect(status().isOk())
         .andExpect(view().name(ConstantsHelper.VIEW_WORKITEMS_CREATE))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEM, isA(WorkItemView.class)))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEM, hasProperty("id", is(0))))
         ;
   }
   
   @Test
   @UnitTest
   @Order(4)
   @MockScrumifyUser(username = "projectdirector")  
   public void listByAllowedUser() throws Exception {
      int EXPECTED_SIZE = 6;
      
      mvc.perform(get(ConstantsHelper.MAPPING_WORKITEMS))
         .andExpect(status().isOk())
         .andExpect(view().name(ConstantsHelper.VIEW_WORKITEMS_READ))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEMS, hasSize(EXPECTED_SIZE)))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEMS, hasItem(allOf(hasProperty("id", is(1))))))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEMS, hasItem(allOf(hasProperty("id", is(2))))))
         ;
   }   
   
   @Test
   @UnitTest
   @MockScrumifyUser(username = "developer1")
   public void updateByNotAllowedUser() throws Exception {
      int workItem = 1;
      
      mvc.perform(get(ConstantsHelper.MAPPING_WORKITEMS_UPDATE + ConstantsHelper.MAPPING_SLASH + workItem))
         .andExpect(status().isForbidden())
         ;
   }
   
   @Test
   @UnitTest
   @MockScrumifyUser(username = "developer3")
   public void updateByAllowedUser() throws Exception {
      int workItem = 1;
      
      mvc.perform(get(ConstantsHelper.MAPPING_WORKITEMS_UPDATE + ConstantsHelper.MAPPING_SLASH + workItem))
         .andExpect(status().isOk())
         ;
   }
   
   @Test
   @UnitTest
   @MockScrumifyUser(username = "developer1")
   public void updateTeamOnNotAllowedUser() throws Exception {
      int workItem = 1;
      int team = 1;
      String url = ConstantsHelper.WORKITEMS_API_MAPPING_UPDATE_TEAM + "/" + workItem + "/" + team;
      
      mvc.perform(get(url))
         .andExpect(status().isForbidden());
   }
   
   

}