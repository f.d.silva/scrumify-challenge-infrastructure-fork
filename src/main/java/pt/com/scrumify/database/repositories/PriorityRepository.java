package pt.com.scrumify.database.repositories;

import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import pt.com.scrumify.database.entities.Priority;

public interface PriorityRepository extends JpaRepository<Priority, Integer> {
   @Cacheable(value = "priorities")
   @Query(nativeQuery = false,
   value = "SELECT p " +
           "FROM Priority p " +
           "ORDER BY p.sortOrder")
   List<Priority> findAll();
}