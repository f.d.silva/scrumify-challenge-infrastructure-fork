package pt.com.scrumify.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import pt.com.scrumify.database.entities.EpicNote;

public interface EpicNoteRepository extends JpaRepository<EpicNote, Integer> {

}