package pt.com.scrumify.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import pt.com.scrumify.database.entities.Environment;


public interface EnvironmentRepository extends JpaRepository<Environment, Integer> {
   Environment findByNameIgnoreCase(String name);
}