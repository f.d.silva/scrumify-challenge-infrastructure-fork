package pt.com.scrumify.database.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_ENVIRONMENTS)
public class Environment implements Serializable {
   private static final long serialVersionUID = 6904399980203715368L;

   @Id
   @Getter
   @Setter
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id", nullable = false)
   private Integer id = 0;
   
   @Getter
   @Setter
   @Column(name = "abbreviation", length = 3, nullable = false)
   private String abbreviation;

   @Getter
   @Setter
   @Column(name = "name", length = 50, nullable = false)
   private String name;   
   
   @Getter
   @Setter
   @OneToMany(fetch = FetchType.LAZY, mappedBy = "environment")
   private List<ApplicationCredential> credentials = new ArrayList<>(0);
   
   @Getter
   @Setter
   @OneToMany(fetch = FetchType.LAZY, mappedBy = "environment")
   private List<ApplicationDataSource> dataSources = new ArrayList<>(0);

   public Environment(int id) {
      super();

      this.id = id;
   }
}