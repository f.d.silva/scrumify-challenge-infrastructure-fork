package pt.com.scrumify.database.entities;

import java.io.Serializable;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_SPRINTMEMBERS)
@AssociationOverrides({ 
   @AssociationOverride(name = "pk.sprint", joinColumns = @JoinColumn(name = "sprint")),
   @AssociationOverride(name = "pk.resource", joinColumns = @JoinColumn(name = "resource")) 
}) 
public class SprintMember implements Serializable {
   private static final long serialVersionUID = 685097620807665016L;

   @Getter
   @Setter
   @EmbeddedId
   private SprintMemberPK pk = new SprintMemberPK();

   @Getter
   @Setter
   @Column(name = "role")
   private String role;

   public SprintMember(SprintMemberPK pk) {
      super();
      
      this.pk = pk;
   }
   
   public SprintMember(Sprint sprint, Resource resource) {
      super();
      
      this.pk.setResource(resource);
      this.pk.setSprint(sprint);
   }
   
   public SprintMember(Sprint sprint, Resource resource, String role) {
      super();
      
      this.pk.setResource(resource);
      this.pk.setSprint(sprint);
      this.role = role;
   }
      
   @Transient
   public Sprint getSprint() {
      return this.pk.getSprint();
   }

   public void setSprint(Sprint sprint) {
      this.pk.setSprint(sprint);
   }

   @Transient
   public Resource getResource() {
      return this.pk.getResource();
   }

   public void setResource(Resource resource) {
      this.pk.setResource(resource);
   }
}