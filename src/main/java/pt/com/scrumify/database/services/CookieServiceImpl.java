package pt.com.scrumify.database.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Cookie;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.repositories.CookieRepository;

@Service
public class CookieServiceImpl implements CookieService {
   @Autowired
   private CookieRepository repository;

   @Override
   public Cookie getOne(String name, Resource resource) {
      return repository.getOne(name, resource);
   }

   @Override
   public Cookie save(Cookie cookie) {
      return repository.save(cookie);
   }
}