package pt.com.scrumify.database.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Application;
import pt.com.scrumify.database.entities.Release;
import pt.com.scrumify.database.repositories.ReleaseRepository;

@Service
public class ReleaseServiceImpl implements ReleaseService {
   @Autowired
   private ReleaseRepository releasesRepository;
   
   @Override
   public Release getOne(int id) {
      return this.releasesRepository.getOne(id);
   }
   
   @Override
   public List<Release> getByApplication(Integer application) {
      return this.releasesRepository.getByApplication(new Application(application));
   }
   
   @Override
   public void delete(Release release) {
      this.releasesRepository.delete(release);
   }
   
   @Override
   public void deleteById(Integer id) {
      this.releasesRepository.deleteById(id);
   }
   
   @Override
   public void save(Release release) {
      this.releasesRepository.save(release);
   }
}