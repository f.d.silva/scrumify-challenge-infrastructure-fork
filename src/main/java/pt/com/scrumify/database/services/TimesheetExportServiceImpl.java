package pt.com.scrumify.database.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.TimesheetExport;
import pt.com.scrumify.database.repositories.TimesheetExportRepository;

@Service
public class TimesheetExportServiceImpl implements TimesheetExportService {
   @Autowired
   private TimesheetExportRepository repository;

   @Override
   public List<TimesheetExport> findByResourcesYearAndMonth(List<String> resources, Integer year, Integer month) {
      return this.repository.findByResourcesYearAndMonth(resources, year, month);
   }

   @Override
   public List<TimesheetExport> findByResourcesYearMonthAndFortnight(List<String> resources, Integer year, Integer month, Integer fortnight) {
      return this.repository.findByResourcesYearMonthAndFortnight(resources, year, month, fortnight);
   }
}