package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.Area;
import pt.com.scrumify.database.entities.Resource;

public interface AreaService {
   Area getOne(Integer id);
   Area save(Area area);
   List<Area> listAll();
   List<Area> getByResource (Resource resource);
}