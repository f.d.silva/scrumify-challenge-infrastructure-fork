package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.TypeOfNote;
import pt.com.scrumify.database.entities.TypeOfWorkItem;

public interface TypeOfWorkItemNoteService {
   TypeOfNote getById(Integer id);
   TypeOfNote save(TypeOfNote typeOfWorkItemNote);
   List<TypeOfNote> getAll();
   List<TypeOfNote> getByTypeOfWorkItem(TypeOfWorkItem typeOfWorkItem);
}