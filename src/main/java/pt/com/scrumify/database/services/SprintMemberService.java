package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Sprint;
import pt.com.scrumify.database.entities.SprintMember;
import pt.com.scrumify.database.entities.SprintMemberPK;


public interface SprintMemberService {
   void delete(SprintMember member);
   SprintMember getOne(SprintMemberPK pk);
   List<SprintMember> getBySprint(Sprint sprint);
   List<SprintMember> getByResource(Resource resource);
   SprintMember save(SprintMember member);
   
}