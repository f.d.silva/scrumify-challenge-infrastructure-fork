package pt.com.scrumify.entities;

import lombok.Getter;
import lombok.Setter;
import pt.com.scrumify.database.entities.Occupation;
import pt.com.scrumify.database.entities.TeamMember;
import pt.com.scrumify.database.entities.TeamMemberPK;

public class TeamMemberView {
   
   @Getter
   @Setter
   private TeamMemberPK pk = new TeamMemberPK();
   
   @Getter
   @Setter
   private Occupation occupation = new Occupation();
   
   @Getter
   @Setter
   private Boolean approver = false;
   
   @Getter
   @Setter
   private Boolean reviewer = false;
   
   @Getter
   @Setter
   private Integer allocation = 100;

   public TeamMemberView() {
      super();  
   }
   
   public TeamMemberView(TeamMember teamMember) {
      super();
      
      if (teamMember != null) {
         this.setPk(teamMember.getPk());
         this.setReviewer(teamMember.isReviewer());
         this.setAllocation(teamMember.getAllocation());
         this.setApprover(teamMember.isApprover());   
         this.setOccupation(teamMember.getOccupation());
      }
   }
   
   public TeamMember translate() {
      TeamMember teamMember = new TeamMember();
      
      teamMember.setAllocation(this.getAllocation());
      teamMember.setApprover(this.getApprover());
      teamMember.setOccupation(this.getOccupation());
      teamMember.setPk(this.getPk());
      teamMember.setReviewer(this.getReviewer());
      
      return teamMember;
   }
}