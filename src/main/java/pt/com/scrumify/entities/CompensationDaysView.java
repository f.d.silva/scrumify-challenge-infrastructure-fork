package pt.com.scrumify.entities;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import pt.com.scrumify.database.entities.CompensationDay;
import pt.com.scrumify.database.entities.Resource;

public class CompensationDaysView {
   
   @Getter
   @Setter
   private Integer id;
   
   @Getter
   @Setter
   private Date startDay;
   
   @Getter
   @Setter
   private Date endDay;
   
   @Getter
   @Setter
   private Date day;
   
   @Getter
   @Setter
   private String name;
   
   @Getter
   @Setter
   private Resource createdBy;
   
   @Getter
   @Setter
   private Date created;
   
   
   public CompensationDaysView() {
      super();
   }
   
   public CompensationDaysView(CompensationDay compensationDay) {      
      this.setId(compensationDay.getId());
      this.setEndDay(compensationDay.getEndDay());
      this.setStartDay(compensationDay.getStartDay());
      this.setDay(compensationDay.getDay().getDate());
      this.setName(compensationDay.getName());
      this.setCreated(compensationDay.getCreated());
      this.setCreatedBy(compensationDay.getCreatedBy());
      
   }

   public CompensationDay translate() {
      CompensationDay compensationDay = new CompensationDay();
      compensationDay.setId(this.getId());
      compensationDay.setEndDay(this.getEndDay());
      compensationDay.setStartDay(this.getStartDay());
      compensationDay.getDay().setDate(this.getDay());
      compensationDay.setName(this.getName());
      compensationDay.setCreated(this.getCreated());
      compensationDay.setCreatedBy(this.getCreatedBy());
      
      return compensationDay;      
   }
}