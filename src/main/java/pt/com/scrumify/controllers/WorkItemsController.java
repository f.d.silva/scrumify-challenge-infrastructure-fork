package pt.com.scrumify.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import io.swagger.annotations.ApiOperation;
import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.database.entities.WorkItem;
import pt.com.scrumify.database.services.ApplicationService;
import pt.com.scrumify.database.services.ReleaseService;
import pt.com.scrumify.database.services.TeamService;
import pt.com.scrumify.database.services.TypeOfWorkItemService;
import pt.com.scrumify.database.services.WorkItemHistoryService;
import pt.com.scrumify.database.services.WorkItemService;
import pt.com.scrumify.database.services.WorkItemStatusService;
import pt.com.scrumify.entities.WorkItemFilterView;
import pt.com.scrumify.entities.WorkItemView;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.ContextHelper;
import pt.com.scrumify.helpers.CookiesHelper;
import pt.com.scrumify.helpers.ResourcesHelper;
import pt.com.scrumify.helpers.WorkItemsHelper;

@Controller
public class WorkItemsController {
   @Autowired
   private ContextHelper contextHelper;
   @Autowired
   private CookiesHelper cookiesHelper;
   @Autowired
   private WorkItemsHelper workItemsHelper;
   @Autowired
   private ApplicationService applicationService;
   @Autowired
   private ReleaseService releasesService;
   @Autowired
   private TeamService teamsService;
   @Autowired
   private TypeOfWorkItemService typeOfWorkitemService;
   @Autowired
   private WorkItemService workItemsService;
   @Autowired
   private WorkItemHistoryService workItemsHistoryService;
   @Autowired
   private WorkItemStatusService itemStatusService;

   @InitBinder
   public void initBinder(WebDataBinder binder) {
      SimpleDateFormat format = new SimpleDateFormat(ConstantsHelper.DEFAULT_FORMAT_DATETIME);
      format.setLenient(true);

      binder.registerCustomEditor(Date.class, new CustomDateEditor(format, true));
   }

   /*
    * LIST WORKITEMS
    */
   @ApiOperation("Mapping to list Workitems.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WORKITEMS_READ + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_WORKITEMS)
   public String list(Model model) {
      WorkItemFilterView filter = workItemsHelper.getFilterFromCookie(ConstantsHelper.COOKIE_WORKITEMS);
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEMS, workItemsHelper.getByFilter(filter));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TYPESOFWORKITEM, typeOfWorkitemService.getAvailable());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_FILTER, filter);
      
      return ConstantsHelper.VIEW_WORKITEMS_READ;
   }

   @GetMapping(value = ConstantsHelper.MAPPING_WORKITEMS_FILTER)
   public String getFilter(Model model) {
      WorkItemFilterView filter = workItemsHelper.getFilterFromCookie(ConstantsHelper.COOKIE_WORKITEMS);
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATIONS, applicationService.listAllByResource(ResourcesHelper.getResource()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAMS, teamsService.getByMember(ResourcesHelper.getResource()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TYPESOFWORKITEM, typeOfWorkitemService.getAvailable());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_FILTER, filter);

      return ConstantsHelper.VIEW_WORKITEMS_FILTER;
   }

   /*
    * SAVE FILTER
    */
   @PostMapping(value = ConstantsHelper.MAPPING_WORKITEMS_FILTER)
   public String submitFilter(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_FILTER) @Valid WorkItemFilterView filter) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEMS, workItemsHelper.getByFilter(filter));
      
      cookiesHelper.save(ConstantsHelper.COOKIE_WORKITEMS, ResourcesHelper.getResource(), filter);

      return ConstantsHelper.VIEW_WORKITEMS_READ_LIST;
   }

   /*
    * CREATE
    */
   @ApiOperation("Mapping to create Workitems.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WORKITEMS_CREATE + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_WORKITEMS_CREATE + ConstantsHelper.MAPPING_PARAMETER_TYPEOFWORKITEM)
   public String create(Model model, @PathVariable Integer typeOfWorkitem) {
      WorkItemView workItemView = new WorkItemView();
      workItemView.setType(this.typeOfWorkitemService.getOne(typeOfWorkitem));

      List<Team> teams = new ArrayList<>();
      teams.add(contextHelper.getActiveTeam());
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEM, workItemView);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATIONS, applicationService.listAllSortedByName());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAMS, teams);

      return ConstantsHelper.VIEW_WORKITEMS_CREATE;
   }

   /*
    * SAVE
    */
   @ApiOperation("Mapping to save Workitems.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WORKITEMS_CREATE + "')")
   @PostMapping(value = ConstantsHelper.MAPPING_WORKITEMS_SAVE)
   public String save(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEM) @Valid WorkItemView workItemView) {
      WorkItem wi = workItemsHelper.save(workItemView);

      return ConstantsHelper.MAPPING_REDIRECT + ConstantsHelper.MAPPING_WORKITEMS_UPDATE + ConstantsHelper.MAPPING_SLASH + wi.getId();
   }

   /*
    * UPDATE
    */
   @ApiOperation("Mapping to update Workitems.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WORKITEMS_UPDATE + "') and @SecurityService.isMember('WorkItem', #workitem)")
   @GetMapping(value = {ConstantsHelper.MAPPING_WORKITEMS_UPDATE + ConstantsHelper.MAPPING_PARAMETER_WORKITEM,
                        ConstantsHelper.MAPPING_WORKITEMS_UPDATE + ConstantsHelper.MAPPING_PARAMETER_WORKITEM + ConstantsHelper.MAPPING_SLASH + ConstantsHelper.MAPPING_PARAMETER_STEPS
                       })
   public String edit(Model model, @PathVariable Integer workitem) {
      WorkItem wi = workItemsService.getOne(workitem);
      workItemsHelper.generateModel(model, wi);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEM, wi);

      return ConstantsHelper.VIEW_WORKITEMS_UPDATE;
   }

   /*
    * AJAX TAB WORKITEM
    */
   @ApiOperation("Mapping to respond ajax requests for workItem information.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WORKITEMS_READ + "') and @SecurityService.isMember('WorkItem', #workitem)")
   @GetMapping(value = ConstantsHelper.MAPPING_WORKITEMS_AJAX + ConstantsHelper.MAPPING_PARAMETER_WORKITEM)
   public String workItemsTab(Model model, @PathVariable int workitem) {
      WorkItem wi = workItemsService.getOne(workitem);
      workItemsHelper.generateModel(model, wi);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEM, wi);

      return ConstantsHelper.VIEW_WORKITEMS_WORKITEM;
   }

   /*
    * AJAX TAB INSTRUCTIONS
    */
   @ApiOperation("Mapping to respond ajax requests for workItem instructions.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WORKITEMS_READ + "') and @SecurityService.isMember('WorkItem', #workitem)")
   @GetMapping(value = ConstantsHelper.MAPPING_WORKITEMS_INSTRUCTIONS_AJAX + ConstantsHelper.MAPPING_PARAMETER_WORKITEM)
   public String instructions(Model model, @PathVariable int workitem) {
      WorkItem wi = workItemsService.getOne(workitem);
      workItemsHelper.generateModel(model, wi);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEM, wi);

      return ConstantsHelper.VIEW_WORKITEMS_INSTRUCTIONS;
   }

   /*
    * UPDATE CONTRACT
    */
   @ApiOperation("Mapping to update Contract in WorkitemTab.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WORKITEMS_UPDATE + "') and @SecurityService.isMember('WorkItem', #workitem)")
   @GetMapping(value = ConstantsHelper.WORKITEMS_API_MAPPING_UPDATE_CONTRACT + ConstantsHelper.MAPPING_PARAMETER_WORKITEM + ConstantsHelper.MAPPING_PARAMETER_CONTRACT)
   public String updateContract(Model model, @PathVariable int workitem, @PathVariable int contract) {
      WorkItem workItem = workItemsHelper.updateContract(workitem, contract);

      workItemsHelper.generateModel(model, workItem);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEM, workItem);

      return ConstantsHelper.VIEW_WORKITEMS_WORKITEM;
   }

   /*
    * UPDATE PRIORITY
    */
   @ApiOperation("Mapping to update contract priority")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WORKITEMS_UPDATE + "') and @SecurityService.isMember('WorkItem', #workitem)")
   @GetMapping(value = ConstantsHelper.WORKITEMS_API_MAPPING_UPDATE_PRIORITY + ConstantsHelper.MAPPING_PARAMETER_WORKITEM + ConstantsHelper.MAPPING_PARAMETER_PRIORITY)
   public String updatePriority(Model model, @PathVariable int workitem, @PathVariable int priority) {
      WorkItem workItem = workItemsHelper.updatePriority(workitem, priority);

      workItemsHelper.generateModel(model, workItem);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEM, workItem);

      return ConstantsHelper.VIEW_WORKITEMS_WORKITEM;
   }

   /*
    * UPDATE STATUS
    */
   @ApiOperation("Mapping to update status in WorkitemTab.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WORKITEMS_UPDATE + "') and @SecurityService.isMember('WorkItem', #workitem)")
   @GetMapping(value = ConstantsHelper.WORKITEMS_API_MAPPING_UPDATE_STATUS + ConstantsHelper.MAPPING_PARAMETER_WORKITEM + ConstantsHelper.MAPPING_PARAMETER_STATE)
   public String updateStatus(Model model, @PathVariable int workitem, @PathVariable int state, HttpServletRequest request, HttpServletResponse response) {
      WorkItem wi = workItemsHelper.updateStatus(workitem, state, request, response);

      workItemsHelper.generateModel(model, wi);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEM, wi);

      return ConstantsHelper.VIEW_WORKITEMS_WORKITEM;
   }

   /*
    * UPDATE ESTIMATE
    */
   @ApiOperation("Mapping to update estimate in Workitem tab.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WORKITEMS_UPDATE + "') and @SecurityService.isMember('WorkItem', #workitem)")
   @GetMapping(value = ConstantsHelper.WORKITEMS_API_MAPPING_UPDATE_ESTIMATE + ConstantsHelper.MAPPING_PARAMETER_WORKITEM + ConstantsHelper.MAPPING_PARAMETER_ESTIMATE)
   public String updateEstimate(Model model, @PathVariable int workitem, @PathVariable int estimate) {
      WorkItem wi = workItemsHelper.updateEstimate(workitem, estimate);

      workItemsHelper.generateModel(model, wi);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEM, wi);

      return ConstantsHelper.VIEW_WORKITEMS_WORKITEM;
   }

   /*
    * UPDATE TEAM
    */
   @ApiOperation("Mapping to update Team in WorkitemTab.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WORKITEMS_CHANGE_TEAMS + "') and @SecurityService.isMember('WorkItem', #workitem)")
   @GetMapping(value = ConstantsHelper.WORKITEMS_API_MAPPING_UPDATE_TEAM + ConstantsHelper.MAPPING_PARAMETER_WORKITEM + ConstantsHelper.MAPPING_PARAMETER_TEAM)
   public String updateTeam(Model model, @PathVariable int workitem, @PathVariable int team) {
      WorkItem wi = workItemsHelper.updateTeam(workitem, team);

      workItemsHelper.generateModel(model, wi);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEM, wi);

      return ConstantsHelper.VIEW_WORKITEMS_WORKITEM;
   }

   /*
    * UPDATE ASSIGNEDTO
    */
   @ApiOperation("Mapping to update AssignedTo in WorkitemTab.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WORKITEMS_READ + "') and @SecurityService.isMember('WorkItem', #workitem)")
   @GetMapping(value = ConstantsHelper.WORKITEMS_API_MAPPING_UPDATE_RESOURCE + ConstantsHelper.MAPPING_PARAMETER_WORKITEM + ConstantsHelper.MAPPING_PARAMETER_RESOURCE)
   public String updateAssignedTo(Model model, @PathVariable int workitem, @PathVariable int resource) {
      WorkItem wi = workItemsHelper.updateAssignedTo(workitem, resource);

      workItemsHelper.generateModel(model, wi);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEM, wi);

      return ConstantsHelper.VIEW_WORKITEMS_WORKITEM;
   }

   /*
    * REMOVE ASSIGNEDTO
    */
   @ApiOperation("Mapping to remove AssignedTo in WorkitemTab.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WORKITEMS_READ + "') and @SecurityService.isMember('WorkItem', #workitem)")
   @GetMapping(value = ConstantsHelper.WORKITEMS_API_MAPPING_REMOVE_RESOURCE + ConstantsHelper.MAPPING_PARAMETER_WORKITEM)
   public String removeAssignedTo(Model model, @PathVariable int workitem) {
      WorkItem wi = workItemsHelper.updateAssignedTo(workitem, 0);

      workItemsHelper.generateModel(model, wi);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEM, wi);

      return ConstantsHelper.VIEW_WORKITEMS_WORKITEM;
   }

   /*
    * UPDATE USERSTORY
    */
   @ApiOperation("Mapping to update UserStory in WorkitemTab.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WORKITEMS_UPDATE + "') and @SecurityService.isMember('WorkItem', #workitem)")
   @GetMapping(value = ConstantsHelper.WORKITEMS_API_MAPPING_UPDATE_USERSTORY + ConstantsHelper.MAPPING_PARAMETER_WORKITEM + ConstantsHelper.MAPPING_PARAMETER_USERSTORY)
   public String updateUserStory(Model model, @PathVariable int workitem, @PathVariable int userStory) {
      WorkItem wi = workItemsHelper.updateUserStory(workitem, userStory);

      workItemsHelper.generateModel(model, wi);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEM, wi);

      return ConstantsHelper.VIEW_WORKITEMS_WORKITEM;
   }

   /*
    * UPDATE APPLICATION
    */
   @ApiOperation("Mapping to update Application in WorkitemTab.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WORKITEMS_UPDATE + "') and @SecurityService.isMember('WorkItem', #workitem)")
   @GetMapping(value = ConstantsHelper.WORKITEMS_API_MAPPING_UPDATE_APPLICATION + ConstantsHelper.MAPPING_PARAMETER_WORKITEM + ConstantsHelper.MAPPING_PARAMETER_APPLICATION)
   public String updateApplication(Model model, @PathVariable int workitem, @PathVariable int app) {
      WorkItem wi = workItemsHelper.updateApplication(workitem, app);

      workItemsHelper.generateModel(model, wi);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEM, wi);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RELEASES, releasesService.getByApplication(app));

      return ConstantsHelper.VIEW_WORKITEMS_WORKITEM;
   }

   /*
    * UPDATE RELEASE VERSION
    */
   @ApiOperation("Mapping to update Application in WorkitemTab.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WORKITEMS_UPDATE + "') and @SecurityService.isMember('WorkItem', #workitem)")
   @GetMapping(value = ConstantsHelper.WORKITEMS_API_MAPPING_UPDATE_RELEASE + ConstantsHelper.MAPPING_PARAMETER_WORKITEM + ConstantsHelper.MAPPING_PARAMETER_RELEASE)
   public String updateRelease(Model model, @PathVariable int workitem, @PathVariable int release) {
      WorkItem wi = workItemsHelper.updateRelease(workitem, release);

      workItemsHelper.generateModel(model, wi);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEM, wi);

      return ConstantsHelper.VIEW_WORKITEMS_WORKITEM;
   }

   /*
    * UPDATE NAME OR DESCRIPTION
    */
   @PostMapping(value = ConstantsHelper.MAPPING_WORKITEMS_INFO_FIELD_UPDATE + ConstantsHelper.MAPPING_PARAMETER_FIELD)
   public String saveField(Model model, @PathVariable String field, @Valid WorkItemView view) {
      WorkItem wi = workItemsHelper.updateField(view, field);

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEM, wi);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_STATUS, itemStatusService.getAll());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCES, workItemsHelper.listOfResourcesToAssign(wi));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CONTRACTS, workItemsHelper.listOfContract(wi));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_STATUS, itemStatusService.getByWorkflow(wi.getType().getWorkflow(), wi.getStatus()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_USERSTORIES, workItemsHelper.listOfUserStory(wi));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCE, ResourcesHelper.getResource());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAMS, workItemsHelper.listOfTeams(wi));

      return ConstantsHelper.VIEW_WORKITEMS_FRAGEMENTUPDATE + field;
   }

   /*
    * WORKITEM HISTORY
    */
   @ApiOperation("Mapping to respond ajax requests for workItem history.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WORKITEMS_READ + "') and @SecurityService.isMember('WorkItem', #workitem)")
   @GetMapping(value = ConstantsHelper.MAPPING_WORKITEMS_HISTORY_AJAX + ConstantsHelper.MAPPING_PARAMETER_WORKITEM)
   public String history(Model model, @PathVariable int workitem) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_IDWORKITEM, workitem);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEMHISTORY, workItemsHistoryService.getWorkItemHistoryByWorkItem(workitem));

      return ConstantsHelper.VIEW_WORKITEMS_HISTORY;
   }

   @GetMapping(value = ConstantsHelper.MAPPING_WORKITEMS_INFO_FIELD + ConstantsHelper.MAPPING_PARAMETER_EPIC + ConstantsHelper.MAPPING_PARAMETER_FIELD + ConstantsHelper.MAPPING_PARAMETER_FIELD_HEADER)
   public String updateField(Model model, @PathVariable int epicId, @PathVariable String field, @PathVariable String fieldheader) {
      WorkItemView workItem = new WorkItemView(workItemsService.getOne(epicId));

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEM, workItem);

      return "common/fragments/fieldedit :: text(label = '" + fieldheader + "', field = '" + field + "', required = false, placeholder = '',columnsize = 'sixteen wide'" + ", form= 'workItemedit-form', object=" + ConstantsHelper.VIEW_ATTRIBUTE_WORKITEM + ")";
   }

   @GetMapping(value = ConstantsHelper.MAPPING_WORKITEMS_SEARCH + ConstantsHelper.MAPPING_PARAMETER_SEARCHFIELD)
   public String search(Model model, @PathVariable String searchField) {
      List<WorkItem> searchResults = null;

      searchResults = workItemsHelper.fuzzySearch(searchField);

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEMS, searchResults);

      return ConstantsHelper.VIEW_WORKITEMS_READ_LIST;
   }
}