package pt.com.scrumify.controllers;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import pt.com.scrumify.database.services.WorkflowService;
import pt.com.scrumify.entities.WorkflowView;
import pt.com.scrumify.helpers.ConstantsHelper;

@Controller
public class WorkflowsController {
   
   @Autowired
   private WorkflowService workflowsService;   
     
   @InitBinder
   public void initBinder(WebDataBinder binder) {
      SimpleDateFormat format = new SimpleDateFormat(ConstantsHelper.DEFAULT_FORMAT_DATETIME);
      format.setLenient(true);
      
      binder.registerCustomEditor(Date.class, new CustomDateEditor(format, true));
   }

   /*
    * LIST
    */   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WORKFLOWS_READ + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_WORKFLOWS)
   public String list(Model model) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKFLOWS , workflowsService.getAll());

      return ConstantsHelper.VIEW_WORKFLOWS_READ;
   }
   
   /*
    * CREATE
    */   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WORKFLOWS_CREATE + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_WORKFLOWS_CREATE)
   public String create(Model model) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKFLOW, new WorkflowView());
      
      return ConstantsHelper.VIEW_WORKFLOWS_SAVE;
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WORKFLOWS_UPDATE + "')")
   @GetMapping(value = { ConstantsHelper.MAPPING_WORKFLOWS_UPDATE + ConstantsHelper.MAPPING_PARAMETER_ID})
   public String edit(Model model, @PathVariable Integer id) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKFLOW, new WorkflowView(workflowsService.getOne(id)));
      return ConstantsHelper.VIEW_WORKFLOWS_UPDATE;
   }
   
   @PostMapping(value = ConstantsHelper.MAPPING_WORKFLOWS_SAVE)
   public String save(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKFLOW) @Valid WorkflowView workFlowView) {
      workflowsService.save(workFlowView.translate());
      
      return ConstantsHelper.MAPPING_REDIRECT + ConstantsHelper.MAPPING_WORKFLOWS;
   }
   
}