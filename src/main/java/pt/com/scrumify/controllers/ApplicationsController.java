package pt.com.scrumify.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import io.swagger.annotations.ApiOperation;
import pt.com.scrumify.database.entities.Application;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.SubArea;
import pt.com.scrumify.database.services.ApplicationStatusService;
import pt.com.scrumify.database.services.TypeOfApplicationService;
import pt.com.scrumify.database.services.ApplicationCredentialService;
import pt.com.scrumify.database.services.ApplicationDataSourceService;
import pt.com.scrumify.database.services.ApplicationLinkService;
import pt.com.scrumify.database.services.ApplicationPropertyService;
import pt.com.scrumify.database.services.ApplicationService;
import pt.com.scrumify.database.services.EnvironmentService;
import pt.com.scrumify.database.services.ResourceService;
import pt.com.scrumify.database.services.SubAreaService;
import pt.com.scrumify.entities.ApplicationView;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.ResourcesHelper;

@Controller
public class ApplicationsController {   
   @Autowired
   private ApplicationService appService;
   @Autowired
   private ApplicationStatusService appStatusService;
   @Autowired
   private TypeOfApplicationService appTypesService;
   @Autowired
   private EnvironmentService environmentsService;
   @Autowired
   private ResourceService resourcesService;
   @Autowired
   private SubAreaService subareaService;
   @Autowired
   private ApplicationCredentialService applicationsCredentialsService;
   @Autowired
   private ApplicationDataSourceService applicationsDataSourceservice;
   @Autowired
   private ApplicationLinkService applicationsLinksService;
   @Autowired
   private ApplicationPropertyService applicationsPropertiesService;
   
   @InitBinder
   public void initBinder(WebDataBinder binder) {
      SimpleDateFormat format = new SimpleDateFormat(ConstantsHelper.DEFAULT_FORMAT_DATETIME);
      format.setLenient(true);
      
      binder.registerCustomEditor(Date.class, new CustomDateEditor(format, true));
   }

   /*
    * LIST
    */
   @ApiOperation("List available applications.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_APPLICATIONS_READ + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_APPLICATIONS)
   public String list(Model model) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATIONS, appService.listAllByResource(ResourcesHelper.getResource()));
      
      return ConstantsHelper.VIEW_APPLICATIONS_READ;
   }

   /*
    * LIST CREDENTIALS, DATA SOURCES, LINKS AND PROPERTIES BY ENVIRONMENT
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_APPLICATIONS_UPDATE + "') and @SecurityService.isMember('Application', #app)")
   @GetMapping(value = ConstantsHelper.MAPPING_APPLICATIONS_ENVIRONMENTS_AJAX + ConstantsHelper.MAPPING_PARAMETER_APPLICATION + ConstantsHelper.MAPPING_PARAMETER_ENVIRONMENT)
   public String list(Model model, @PathVariable int app, @PathVariable int environment) {
      Application application = this.appService.getOne(app);
      ApplicationView view = new ApplicationView(application);
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATION, view);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATION_TYPESOFDETAIL, ConstantsHelper.TypesOfApplicationDetail.values());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_ENVIRONMENT, environment);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TYPESOFCREDENTIAL, ConstantsHelper.TypesOfCredential.values());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CREDENTIALS, applicationsCredentialsService.getByApplicationAndEnvironment(app, environment));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_DATASOURCES, applicationsDataSourceservice.getByApplicationAndEnvironment(app, environment));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_LINKS, applicationsLinksService.getByApplicationAndEnvironment(app, environment));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_PROPERTIES, applicationsPropertiesService.getByApplicationAndEnvironment(app, environment));

      return ConstantsHelper.VIEW_APPLICATIONS_PROPERTIES;
   }
   
   /*
    * CREATE
    */
   @ApiOperation("Create a new application.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_APPLICATIONS_CREATE + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_APPLICATIONS_CREATE)
   public String create(Model model) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATION , new ApplicationView());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATIONSTATUS , appStatusService.listAll());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATION_TYPES , appTypesService.listAll());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCES , new ArrayList<Resource>());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_EXECUTIVES , new ArrayList<Resource>());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SUBAREAS , subareaService.listSubareasByResource(ResourcesHelper.getResource()));
      
      return ConstantsHelper.VIEW_APPLICATIONS_CREATE;
   }
   
   /*
    * UPDATE
    */
   @ApiOperation("Update an existing application.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_APPLICATIONS_UPDATE + "') and @SecurityService.isMember('Application', #app)")
   @GetMapping(value = { ConstantsHelper.MAPPING_APPLICATIONS_UPDATE + ConstantsHelper.MAPPING_PARAMETER_APPLICATION, ConstantsHelper.MAPPING_APPLICATIONS_UPDATE + ConstantsHelper.MAPPING_PARAMETER_APPLICATION + ConstantsHelper.MAPPING_SLASH + "{steps}"})
   public String update(Model model, @PathVariable int app, @PathVariable Optional<Integer> steps) {  
      ApplicationView applicationview = new ApplicationView(appService.getOne(app));
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATION , applicationview);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATIONSTATUS , appStatusService.listAll());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATION_TYPES , appTypesService.listAll());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCES , resourcesService.getBySubArea(applicationview.getSubArea()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_EXECUTIVES , resourcesService.getApproversBySubArea(applicationview.getSubArea()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SUBAREAS , subareaService.listSubareasByResource(ResourcesHelper.getResource()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATION_TYPESOFDETAIL, ConstantsHelper.TypesOfApplicationDetail.values());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_ENVIRONMENTS, this.environmentsService.getAll());
      if(steps.isPresent() && steps.get() != 0){
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_PROPERTIES , applicationsPropertiesService.getByApplicationAndEnvironment(app, steps.get()));
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CREDENTIALS , applicationsCredentialsService.getByApplicationAndEnvironment(app, steps.get()));
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_DATASOURCES , applicationsDataSourceservice.getByApplicationAndEnvironment(app, steps.get()));
      }
      
      return ConstantsHelper.VIEW_APPLICATIONS_CREATE;
   }
   
   /*
    * SAVE
    */
   @ApiOperation("Save current application.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_APPLICATIONS_UPDATE + "')")
   @PostMapping(value = ConstantsHelper.MAPPING_APPLICATIONS_SAVE)
   public String save(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATION) @Valid ApplicationView applicationView, BindingResult bindingResult) {
      if (bindingResult.hasErrors()) {
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATION , applicationView);
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATIONSTATUS , appStatusService.listAll());
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATION_TYPES , appTypesService.listAll());
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCES , new ArrayList<Resource>());
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_EXECUTIVES , new ArrayList<Resource>());
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SUBAREAS , subareaService.listSubareasByResource(ResourcesHelper.getResource()));
         
         return ConstantsHelper.VIEW_FRAGMENT_APPLICATIONS_INFO;
      }
      
      this.appService.save(applicationView.translate());

      return ConstantsHelper.MAPPING_REDIRECT + ConstantsHelper.MAPPING_APPLICATIONS;
   }

   /*
    * TAB INFO
    */
   @ApiOperation("Mapping to respond ajax requests for info (application forms).")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_APPLICATIONS_UPDATE + "') and @SecurityService.isMember('Application', #app)")
   @GetMapping( value =  ConstantsHelper.MAPPING_APPLICATIONS_AJAX + ConstantsHelper.MAPPING_PARAMETER_APPLICATION )
   public String infoTab(Model model, @PathVariable int app) {
      ApplicationView view = new ApplicationView(appService.getOne(app));
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATION , view);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATIONSTATUS , appStatusService.listAll());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATION_TYPES , appTypesService.listAll());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCES , resourcesService.getBySubArea(view.getSubArea()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_EXECUTIVES , resourcesService.getApproversBySubArea(view.getSubArea()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SUBAREAS , subareaService.listSubareasByResource(ResourcesHelper.getResource()));
      
    return ConstantsHelper.VIEW_FRAGMENT_APPLICATIONS_INFO;
   }
   
   @GetMapping(value = ConstantsHelper.MAPPING_APPLICATIONS_SUBAREA_UPDATE + ConstantsHelper.MAPPING_PARAMETER_ID)
   public String updateResources(Model model, @PathVariable int id) {
      
      SubArea subArea = subareaService.getOne(id);
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATION , new ApplicationView());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCES , resourcesService.getBySubArea(subArea));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_EXECUTIVES , resourcesService.getApproversBySubArea(subArea));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATIONSTATUS , appStatusService.listAll());
      
      return ConstantsHelper.VIEW_FRAGMENT_APPLICATIONS_RESOURCES;
   }

}