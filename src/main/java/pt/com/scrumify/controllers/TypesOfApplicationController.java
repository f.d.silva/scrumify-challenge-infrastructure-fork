package pt.com.scrumify.controllers;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;

import io.swagger.annotations.ApiOperation;
import pt.com.scrumify.database.entities.TypeOfApplication;
import pt.com.scrumify.database.services.TypeOfApplicationService;
import pt.com.scrumify.helpers.ConstantsHelper;

@Controller
public class TypesOfApplicationController {
   
   @Autowired
   private TypeOfApplicationService typeOfApplicationService;
      
   @InitBinder
   public void initBinder(WebDataBinder binder) {
      SimpleDateFormat format = new SimpleDateFormat(ConstantsHelper.DEFAULT_FORMAT_DATETIME);
      format.setLenient(true);      
      binder.registerCustomEditor(Date.class, new CustomDateEditor(format, true));
   }

   @ApiOperation("List available applications types.")
   @GetMapping(value = ConstantsHelper.MAPPING_APPLICATIONS_TYPES)
   public String list(Model model) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATION_TYPES, typeOfApplicationService.listAll());
      
      return ConstantsHelper.VIEW_APPLICATIONS_TYPES_READ;
   }
   
   @ApiOperation("Create a new application type.")
   @GetMapping(value = ConstantsHelper.MAPPING_APPLICATIONS_TYPES_CREATE)
   public String create(Model model) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATION_TYPE , new TypeOfApplication());      
      
      return ConstantsHelper.VIEW_APPLICATIONS_TYPES_CREATE;
   }
   
   @ApiOperation("Save current application type.")
   @PostMapping(value = ConstantsHelper.MAPPING_APPLICATIONS_TYPES_SAVE)
   public String save(Model model, BindingResult bindingResult) {
      
      return ConstantsHelper.MAPPING_REDIRECT + ConstantsHelper.MAPPING_APPLICATIONS_TYPES;
   }
   
}