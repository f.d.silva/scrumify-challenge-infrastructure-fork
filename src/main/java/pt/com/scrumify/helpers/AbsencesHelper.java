package pt.com.scrumify.helpers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pt.com.scrumify.database.entities.Absence;
import pt.com.scrumify.database.entities.Timesheet;
import pt.com.scrumify.database.entities.TypeOfWork;
import pt.com.scrumify.database.services.AbsenceService;
import pt.com.scrumify.database.services.TimeService;
import pt.com.scrumify.database.services.TimesheetService;
import pt.com.scrumify.entities.AbsenceView;

@Service
public class AbsencesHelper {
   @Autowired
   private AbsenceService absencesService;
   @Autowired
   private TimeService timesService;
   @Autowired
   private TimesheetService timesheetService;
   @Autowired
   private TimesheetDayHelper timesheetDayHelper;
   @Autowired
   private TimesheetReviewHelper timesheetReviewHelper;

   @Transactional
   public Absence add(AbsenceView view) {      
      Absence absence = view.translate();
      absence.setResource(ResourcesHelper.getResource());
      absence.setDay(timesService.getOne(absence.getDay().getId()));

      /*
       * Get timesheet
       */
      Timesheet timesheet = timesheetService.getSheetByFortnight(absence.getResource(), absence.getDay().getYear().getId(), absence.getDay().getMonth(), absence.getDay().getFortnight());
      
      absence.setTimesheet(timesheet);
      
      /*
       * Check if is a new worklog or worklog update
       */
      if (view.getId() == 0) {
         /*
          * Save absence
          */
         absence = absencesService.save(absence);
         
         /*
          * Add hours to timesheet day
          */
         timesheetDayHelper.addAbsence(absence);
         
         /*
          * Add hours to timesheet review
          */
         timesheetReviewHelper.add(TypeOfWork.OTHER, absence.getHours(), absence.getDay().getYear(), absence.getDay().getMonth(), absence.getDay().getFortnight(), null, absence.getType().getCode(), absence.getType().getName());
      }
      else {
         Absence previousAbsence = absencesService.getById(view.getId()).clone();
         
         /*
          * Save absence
          */
         absence = absencesService.save(absence);
         
         /*
          * Adjust hours on timesheet day (decrement previous value and increment the new value)
          */
         timesheetDayHelper.removeAbsence(previousAbsence);
         timesheetDayHelper.addAbsence(absence);
         
         /*
          * Adjust hours on timesheet review (decrement previous value and increment the new value)
          */
         timesheetReviewHelper.remove(TypeOfWork.OTHER, previousAbsence.getDay().getYear(), previousAbsence.getDay().getMonth(), previousAbsence.getDay().getFortnight(), previousAbsence.getType().getCode(), previousAbsence.getType().getName(), previousAbsence.getHours());
         timesheetReviewHelper.add(TypeOfWork.OTHER, absence.getHours(), absence.getDay().getYear(), absence.getDay().getMonth(), absence.getDay().getFortnight(), null, absence.getType().getCode(), absence.getType().getName());
      }
      
      return absence;
   }

//   @Transactional
//   public void approve(Timesheet timesheet) {
//      List<Absence> absences = absencesService.getByTimesheet(timesheet.getResource(), timesheet.getStartingDay().getYear(), timesheet.getStartingDay().getMonth(), timesheet.getStartingDay().getFortnight());
//      
//      for(Absence absence : absences)
//         absence.setTimesheet(timesheet);
//      
//      absencesService.saveAll(absences);
//   }
   
   @Transactional
   public Absence remove(Integer id) {
      Absence absence = absencesService.getById(id);
      
      /*
       * Decrease hours on timesheet day
       */
      timesheetDayHelper.removeAbsence(absence);
      
      /*
       * Decrease hours on timesheet review
       */
      timesheetReviewHelper.remove(TypeOfWork.OTHER, absence.getDay().getYear(), absence.getDay().getMonth(), absence.getDay().getFortnight(), absence.getType().getCode(), absence.getType().getName(), absence.getHours());
      
      /*
       * Delete absence
       */
      absencesService.delete(absence);
      
      return absence;
   }
}