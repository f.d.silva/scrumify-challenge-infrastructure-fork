$(document).ready(function() {   
  initialize();
});

function initialize() {
  var json = getjson([[@{${T(pt.com.scrumify.helpers.ConstantsHelper).SCRIPT_CALENDAR_LOCALIZATION_JSON}} + ${#locale.language} + '.json']]);

  var date = $("input[type=hidden][id='dateapproved']").val();
  var limit = $("input[type=hidden][id='limit']").val();
  calendar('day', 'date', json, date, limit);
	
  $('.ui.radio.checkbox').checkbox();
  $('.ui.toggle.checkbox').checkbox();
  $('.ui.dropdown').dropdown({
    ignoreDiacritics: true,
    sortSelect: true,
    fullTextSearch:'exact'
  });

  formValidation();
}

function formSubmission() {	
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WORKITEMS_WORKLOG_SAVE}}]];
   
  ajaxpost(url, $("#workitemworklog-form").serialize(), "", false, function() {
    initialize();
  });
}

function formValidation() {
  $("#workitemworklog-form").form({
    on : 'blur',
    inline : true,
    fields : {
      timeSpent : {
        identifier : 'timeSpent',
        rules : [ {
          type : 'integer[1..9]',
          prompt : [[#{validation.empty.range(#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY_RANGE}}, 1, 9)}]]
        } ]
      },
      etc : {
        identifier : 'etc',
        rules : [ {
          type : 'integer[0..99]',
          prompt : [[#{validation.empty.range(#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY_RANGE}}, 0, 99)}]]
        } ]
      },
      typeOfWork : {
        identifier : 'typeOfWork.id',
        rules : [ {
          type : 'empty',
          prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
        } ]
      },
      day : {
        identifier : 'day',
        rules : [ {
          type : 'empty',
          prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
        } ]
      }, 
      timesheet : {
        identifier : 'day',
          rules : [ {
            type : 'timesheet',
            prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_TIMESHEET}}]],
            value: "input[id='input-day']"			            	
          }]
        }
      },
      onSuccess : function(event) {
        event.preventDefault();

        document.forms["workitemworklog-form"].submit( function(data) {
          $('body').html(data);
        }
      );
    }
  });
}

function createWorklog(id) {	
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WORKITEMS_WORKLOG_NEW} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id;
    
  ajaxget(url, "div[id='modal']", function() {
    initialize();
  }, true);
}

function editWorklog(id) {	
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WORKITEMS_WORKLOG_UPDATE} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id;
    
  ajaxget(url, "div[id='modal']", function() {
    initialize();
  }, true);
}

function deleteWorklog(id) {	
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WORKITEMS_WORKLOG_DELETE} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id;
	
  $("#modal-confirmation").modal({
    closable: false,
    onApprove: function() {
      ajaxget(url, "div[id='worklog']", function() {
        initialize();
      }, false);
    },
    onDeny: function() {
    }
  })
  .modal('show');  
}
